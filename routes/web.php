<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Session;
use Barryvdh\DomPDF\Facade as PDF;


Route::get('/error-test', function () {
    Log::info('calling the error route');
    throw new \Exception('something unexpected broke');
});

Route::get('/maintenance', function () {
    Artisan::call('down');
});

Route::get('/production', function () {
    Artisan::call('up');
});

Route::post('/submit-form', function () {
    //
})->middleware(\Spatie\HttpLogger\Middlewares\HttpLogger::class);


Route::group(['namespace' => 'Barang'], function () {

    Route::group(['middleware' => 'authLogin'], function () {

        // Barang
        Route::get('/barang', "Barang@index")->name('barangList');
        Route::post('/barang/datatable', 'Barang@data_table')->name('barangDataTable');
        Route::post('/barang', "Barang@insert")->name('barangInsert');
        Route::delete('/barang/{id}', "Barang@delete")->name('barangDelete');
        Route::get('/barang/{id}', "Barang@edit_modal")->name('barangEditModal');
        Route::post('/barang/{id}', "Barang@update")->name('barangUpdate');

        /**
         * Untuk update harga barang jual secara random
         */
        Route::get('/barang-harga-update', function() {
            $barang = \app\Models\mBarang::get();
            foreach($barang as $row) {
//                \Illuminate\Support\Facades\DB::table('barang')->where('id_barang', $row->id_barang)->update(['brg_harga_jual' => rand(3000, 19000)]);
            }
        });

        // Stok Barang
        Route::get('/stok-barang/{id_barang}', "StokBarang@list")->name('stokBarangList');
        Route::post('/stok-barang/{id_barang}/datatable', 'StokBarang@data_table')->name('stokBarangDataTable');
        Route::get('/stok-barang/{id_barang}/{id_stok_barang}', "StokBarang@edit_modal")->name('stokBarangEditModal');
        Route::post('/stok-barang/{id_barang}/{id_stok_barang}', "StokBarang@update")->name('stokBarangUpdate');
    });

});

Route::group(['namespace' => 'Pembelian'], function () {

    Route::group(['middleware' => 'authLogin'], function () {
        Route::get('/pembelian', 'Pembelian@index')->name('pembelianList');
        Route::post('/pembelian/datatable', 'Pembelian@data_table')->name('pembelianDataTable');
        Route::get('/pembelian/create', 'Pembelian@create')->name('pembelianCreate');
        Route::post('/pembelian/no_faktur', 'Pembelian@no_faktur')->name('pembelianNoFaktur');
        Route::post('/pembelian/insert', 'Pembelian@insert')->name('pembelianInsert');

        Route::get('/pembelian/posting/{id_posting}', 'Pembelian@proses_posting')->name('pembelianProsesPosting');

        Route::get('/pembelian/edit/harga/{id_pembelian}', 'Pembelian@edit_harga')->name('pembelianEditHarga');
        Route::get('/pembelian/edit/stok/{id_pembelian}', 'Pembelian@edit_stok')->name('pembelianEditStok');
        Route::post('/pembelian/update/harga/{id_pembelian}', 'Pembelian@update_harga')->name('pembelianUpdateHarga');
        Route::post('/pembelian/update/stok/{id_pembelian}', 'Pembelian@update_stok')->name('pembelianUpdateStok');
        Route::get('/pembelian/detail/modal/{id_pembelian}', 'Pembelian@detail_modal')->name('pembelianDetailModal');

        Route::get('/pembelian/download/pdf', "Pembelian@download_pdf")->name('pembelianDownloadPdf');
        Route::get('/pembelian/download/excel', "Pembelian@download_excel")->name('pembelianDownloadExcel');

        Route::delete('/pembelian/{id_pembelian}', "Pembelian@delete")->name('pembelianDelete');

    });

});

Route::group(['namespace' => 'Penjualan'], function () {

    Route::group(['middleware' => 'authLogin'], function () {
        Route::get('/penjualan', 'Penjualan@index')->name('penjualanList');
        Route::post('/penjualan/datatable', 'Penjualan@data_table')->name('penjualanDataTable');
        Route::get('/penjualan/create', 'Penjualan@create')->name('penjualanCreate');
        Route::post('/penjualan/insert', 'Penjualan@insert')->name('penjualanInsert');
        Route::delete('/penjualan/delete/{id_penjualan}', 'Penjualan@delete')->name('penjualanDelete');

        Route::get('/penjualan/posting/{id_posting}', 'Penjualan@proses_posting')->name('penjualanProsesPosting');

        Route::post('/penjualan/no_faktur', 'Penjualan@no_faktur')->name('penjualanNoFaktur');
        Route::post('/penjualan/stok_barang', 'Penjualan@stok_barang')->name('penjualanStokBarang');

        Route::get('/penjualan/edit/harga/{id_penjualan}', 'Penjualan@edit_harga')->name('penjualanEditHarga');
        Route::get('/penjualan/edit/stok/{id_penjualan}', 'Penjualan@edit_stok')->name('penjualanEditStok');

        Route::post('/penjualan/update/harga/{id_penjualan}', 'Penjualan@update_harga')->name('penjualanUpdateHarga');
        Route::post('/penjualan/update/stok/{id_penjualan}', 'Penjualan@update_stok')->name('penjualanUpdateStok');

        Route::delete('/penjualan/delete/{id_penjualan}', 'Penjualan@delete')->name('penjualanDelete');

        Route::get('/penjualan/detail/modal/{id_penjualan}', 'Penjualan@detail_modal')->name('penjualanDetailModal');
        Route::get('/penjualan/invoice/print/{id_penjualan}', 'Penjualan@invoice_print')->name('penjualanInvoicePrint');

        Route::get('/penjualan/return/{id_penjualan}', 'Penjualan@return_page')->name('penjualanReturnPage');
        Route::post('/penjualan/return/process/{id_penjualan}', 'Penjualan@return_process')->name('penjualanReturnProcess');
    });

});

Route::group(['namespace' => 'ReturBarang'], function () {

    Route::group(['middleware' => 'authLogin'], function () {

        Route::get('/retur-barang', 'ReturBarang@index')->name('returBarangList');
        Route::post('/retur-barang/datatable', 'ReturBarang@data_table')->name('returBarangDataTable');

        Route::get('/retur-barang/create', 'ReturBarang@create')->name('returBarangCreate');
        Route::post('/retur-barang/insert', 'ReturBarang@insert')->name('returBarangInsert');
        Route::get('/retur-barang/print/nota/{id_retur_barang}', 'ReturBarang@nota_retur_barang')->name('returBarangPrintNota');

        Route::post('/retur-barang/penjualan/datatable', 'ReturBarang@data_table_penjualan')->name('returBarangPenjualanDataTable');
        Route::post('/retur-barang/no-faktur', 'ReturBarang@no_faktur')->name('returBarangNoFaktur');
        Route::post('/retur-barang/penjualan-barang-list', 'ReturBarang@penjualan_barang_list')->name('returPenjualanBarangList');


//        Route::post('/retur-barang/insert', 'ReturBarang@insert')->name('returBarangInsert');
        Route::get('/retur-barang/detail/{id_retur_barang}', 'ReturBarang@detail_modal')->name('returBarangDetailModal');
//        Route::get('/retur-barang/cetak/{id_retur_barang}', 'ReturBarang@cetak')->name('returBarangCetak');
    });

});

Route::group(['namespace' => 'PenyesuaianStok'], function () {

    Route::group(['middleware' => 'authLogin'], function () {
        Route::get('/penyesuaian-stok-barang', 'PenyesuaianStok@index')->name('penyesuaianStokList');
        Route::post('/penyesuaian-stok-barang/datatable', 'PenyesuaianStok@data_table')->name('penyesuaianStokDataTable');
        Route::get('/penyesuaian-stok-barang/modal/{id_barang}', 'PenyesuaianStok@penyesuaian_modal')->name('penyesuaianStokModal');
        Route::post('/penyesuaian-stok-barang/update', 'PenyesuaianStok@update')->name('penyesuaianStokUpdate');

        Route::get('/penyesuaian-stok-barang/history', 'PenyesuaianStok@index_history')->name('penyesuaianStokHistoryList');
        Route::post('/penyesuaian-stok-barang/history/datatable', 'PenyesuaianStok@data_table_history')->name('penyesuaianStokHistoryDataTable');
    });

});

Route::group(['namespace' => 'KartuStok'], function () {
    Route::group(['middleware' => 'authLogin'], function () {
        Route::get('/kartu-stok', 'KartuStok@index')->name('kartuStokList');
        Route::post('/kartu-stok/datatable', 'KartuStok@data_table')->name('kartuStokDataTable');
    });

});

Route::group(['namespace' => 'StokAlert'], function () {
    Route::group(['middleware' => 'authLogin'], function () {
        Route::get('/stok-alert', 'StokAlert@index')->name('stokAlertList');
        Route::post('/stok-alert/datatable', 'StokAlert@data_table')->name('stokAlertDataTable');
    });
});

Route::group(['namespace' => 'HutangPiutang'], function () {
    Route::group(['middleware' => 'authLogin'], function () {


        /**
         * Hutang Lain Lain
         */
        Route::get('/hutang-lain', 'HutangLain@index')->name('hutangLainList');
        Route::post('/hutang-lain/datatable', 'HutangLain@data_table')->name('hutangLainDataTable');
        Route::post('/hutang-lain/insert', 'HutangLain@insert')->name('hutangLainInsert');
        Route::delete('/hutang-lain/delete/{id_hutang_lain}', 'HutangLain@delete')->name('hutangLainDelete');

        Route::get('/hutang-lain/pembayaran/modal/{id_hutang_lain}', 'HutangLain@pembayaran_modal')->name('hutangLainPembayaranModal');
        Route::post('/hutang-lain/pembayaran', 'HutangLain@pembayaran_insert')->name('hutangLainPembayaranInsert');
        Route::get('/hutang-lain/pembayaran/modal/history/{id_hutang_lain}', 'HutangLain@pembayaran_history_modal')->name('hutangLainPembayaranHistoryModal');

        Route::get('/hutang-lain/lunas', 'HutangLain@index_lunas')->name('hutangLainLunasList');
        Route::post('/hutang-lain/lunas/datatable', 'HutangLain@data_table_lunas')->name('hutangLainLunasDataTable');

        /**
         * Hutang Supplier
         */
        Route::get('/hutang-supplier', 'HutangSupplier@index')->name('hutangSupplierList');
        Route::post('/hutang-supplier/datatable', 'HutangSupplier@data_table')->name('hutangSupplierDataTable');
        Route::get('/hutang-supplier/pembayaran/modal/{id_hutang_supplier}', 'HutangSupplier@pembayaran_modal')->name('hutangSupplierPembayaranModal');
        Route::post('/hutang-supplier/pembayaran', 'HutangSupplier@pembayaran_insert')->name('hutangSupplierPembayaranInsert');
        Route::get('/hutang-supplier/pembayaran/modal/history/{id_hutang_supplier}', 'HutangSupplier@pembayaran_history_modal')->name('hutangSupplierPembayaranHistoryModal');

        Route::get('/hutang-supplier/lunas', 'HutangSupplier@index_lunas')->name('hutangSupplierLunasList');
        Route::post('/hutang-supplier/lunas/datatable', 'HutangSupplier@data_table_lunas')->name('hutangSupplierLunasDataTable');


        /**
         * Piutang Lain Lain
         */
        Route::get('/piutang-lain', 'PiutangLain@index')->name('piutangLainList');
        Route::post('/piutang-lain/datatable', 'PiutangLain@data_table')->name('piutangLainDataTable');
        Route::post('/piutang-lain/insert', 'PiutangLain@insert')->name('piutangLainInsert');
        Route::delete('/piutang-lain/delete/{id_piutang_lain}', 'PiutangLain@delete')->name('piutangLainDelete');

        Route::get('/piutang-lain/pembayaran/modal/{id_piutang_lain}', 'PiutangLain@pembayaran_modal')->name('piutangLainPembayaranModal');
        Route::post('/piutang-lain/pembayaran', 'PiutangLain@pembayaran_insert')->name('piutangLainPembayaranInsert');
        Route::get('/piutang-lain/pembayaran/modal/history/{id_piutang_lain}', 'PiutangLain@pembayaran_history_modal')->name('piutangLainPembayaranHistoryModal');

        Route::get('/piutang-lain/lunas', 'PiutangLain@index_lunas')->name('piutangLainLunasList');
        Route::post('/piutang-lain/lunas/datatable', 'PiutangLain@data_table_lunas')->name('piutangLainLunasDataTable');

        /**
         * Piutang Pelanggan
         */
        Route::get('/piutang-pelanggan', 'PiutangPelanggan@index')->name('piutangPelangganList');
        Route::post('/piutang-pelanggan/datatable', 'PiutangPelanggan@data_table')->name('piutangPelangganDataTable');
        Route::get('/piutang-pelanggan/pembayaran/modal/{id_piutang_pelanggan}', 'PiutangPelanggan@pembayaran_modal')->name('piutangPelangganPembayaranModal');
        Route::post('/piutang-pelanggan/pembayaran', 'PiutangPelanggan@pembayaran_insert')->name('piutangPelangganPembayaranInsert');
        Route::get('/piutang-pelanggan/pembayaran/modal/history/{id_piutang_pelanggan}', 'PiutangPelanggan@pembayaran_history_modal')->name('piutangPelangganPembayaranHistoryModal');

        Route::get('/piutang-pelanggan/lunas', 'PiutangPelanggan@index_lunas')->name('piutangPelangganLunasList');
        Route::post('/piutang-pelanggan/lunas/datatable', 'PiutangPelanggan@data_table_lunas')->name('piutangPelangganLunasDataTable');
    });
});


Route::group(['namespace' => 'Laporan'], function () {
    Route::group(['middleware' => 'authLogin'], function () {
        Route::get('/laporan', 'Laporan@index')->name('laporanList');
        Route::post('/laporan/pembelian/datatable', 'Laporan@data_table_pembelian')->name('laporanPembelianDataTable');
        Route::post('/laporan/penjualan/datatable', 'Laporan@data_table_penjualan')->name('laporanPenjualanDataTable');

        Route::get('/laporan/cetak/pdf', 'Laporan@cetak_pdf')->name('laporanCetakPdf');
    });
});

Route::namespace('General')->group(function () {

    Route::get('undercosntruction', "Underconstruction@index")->name('underconstructionPage');
    Route::post('city/list', "General@city_list")->name('cityList');
    Route::post('subdistrict/list', "General@subdistrict_list")->name('subdistrictList');

    Route::group(['middleware' => 'checkLogin'], function () {
        Route::get('/', "Login@index")->name("loginPage");
        Route::post('/roles/login', 'Login@roles')->name("rolesLogin");
        Route::post('/login', "Login@do")->name("loginDo");
    });

    Route::group(['middleware' => 'authLogin'], function () {
        Route::get('/dashboard', "Dashboard@index")->name('dashboardPage');
        Route::get('/whatsapp-test', "Dashboard@whatsapp_test")->name('whatsappTest');
        Route::get('/logout', "Logout@do")->name('logoutDo');
        Route::get('/profile', "Profile@index")->name('profilPage');
        Route::post('/profile/administrator', "Profile@update_administrator")->name('profilAdministratorUpdate');

        Route::post('/cetak/pdf', "General@cetak_pdf")->name('cetakPdf');

    });
});

// =============== Master Data =================== //

Route::group(['namespace' => 'MasterData', 'middleware' => 'authLogin'], function () {

    // Satuan
    Route::get('/satuan', "Satuan@index")->name('satuanList');
    Route::post('/satuan/datatable', 'Satuan@data_table')->name('satuanDataTable');
    Route::post('/satuan', "Satuan@insert")->name('satuanInsert');
    Route::delete('/satuan/{id}', "Satuan@delete")->name('satuanDelete');
    Route::get('/satuan/{id}', "Satuan@edit_modal")->name('satuanEditModal');
    Route::post('/satuan/{id}', "Satuan@update")->name('satuanUpdate');

    // Jenis Barang
    Route::get('/jenis-barang', "JenisBarang@index")->name('jenisBarangList');
    Route::post('/jenis-barang/datatable', 'JenisBarang@data_table')->name('jenisBarangDataTable');
    Route::post('/jenis-barang', "JenisBarang@insert")->name('jenisBarangInsert');
    Route::delete('/jenis-barang/{id}', "JenisBarang@delete")->name('jenisBarangDelete');
    Route::get('/jenis-barang/{id}', "JenisBarang@edit_modal")->name('jenisBarangEditModal');
    Route::post('/jenis-barang/{id}', "JenisBarang@update")->name('jenisBarangUpdate');

    // Supplier
    Route::get('/supplier', "Supplier@index")->name('supplierList');
    Route::post('/supplier/datatable', 'Supplier@data_table')->name('supplierDataTable');
    Route::post('/supplier', "Supplier@insert")->name('supplierInsert');
    Route::delete('/supplier/{id}', "Supplier@delete")->name('supplierDelete');
    Route::get('/supplier/{id}', "Supplier@edit_modal")->name('supplierEditModal');
    Route::post('/supplier/{id}', "Supplier@update")->name('supplierUpdate');

    // Pelanggan
    Route::get('/pelanggan', "Pelanggan@index")->name('pelangganList');
    Route::post('/pelanggan/datatable', 'Pelanggan@data_table')->name('pelangganDataTable');
    Route::post('pelanggan', "Pelanggan@insert")->name('pelangganInsert');
    Route::delete('/pelanggan/{id}', "Pelanggan@delete")->name('pelangganDelete');
    Route::get('/pelanggan/{id}', "Pelanggan@edit_modal")->name('pelangganEditModal');
    Route::post('/pelanggan/{id}', "Pelanggan@update")->name('pelangganUpdate');

    // Karyawan
    Route::get('/karyawan', "Karyawan@index")->name('karyawanPage');
    Route::post('/karyawan/datatable', 'Karyawan@data_table')->name('karyawanDataTable');
    Route::post('/karyawan', "Karyawan@insert")->name('karyawanInsert');
    Route::get('/karyawan-list', "Karyawan@list")->name('karyawanList');
    Route::delete('/karyawan/{id}', "Karyawan@delete")->name('karyawanDelete');
    Route::get('/karyawan/{id}', "Karyawan@edit_modal")->name('karyawanEditModal');
    Route::post('/karyawan/{id}', "Karyawan@update")->name('karyawanUpdate');

    // User
    Route::get('/user', "User@index")->name('userPage');
    Route::post('/user/datatable', 'User@data_table')->name('userDataTable');
    Route::post('/user', "User@insert")->name('userInsert');
    Route::get('/user-list', "User@list")->name('userList');
    Route::delete('/user/{kode}', "User@delete")->name('userDelete');
    Route::get('/user/modal/{kode}', "User@edit_modal")->name('userEditModal');
    Route::post('/user/{kode}', "User@update")->name('userUpdate');

    // Role User
    Route::get('/user-role', "UserRole@index")->name('userRolePage');
    Route::post('/user-role/datatable', 'UserRole@data_table')->name('userRoleDataTable');
    Route::post('/user-role', "UserRole@insert")->name('userRoleInsert');
    Route::delete('/user-role/{id}', "UserRole@delete")->name('userRoleDelete');
    Route::get('/user-role/modal/{id}', "UserRole@edit")->name('userRoleEditModal');
    Route::post('/user-role/{id}', "UserRole@update")->name('userRoleUpdate');
    Route::get('/user-role/akses/{id}', "UserRole@akses")->name('userRoleAkses');
    Route::post('/user-role/akses/{id}', "UserRole@akses_update")->name('userRoleAksesUpdate');

    // Metode Tindakan
    Route::get('/metode-tindakan', "ActionMethod@index")->name('actionMethodPage');
    Route::post('/metode-tindakan', "ActionMethod@insert")->name('actionMethodInsert');
    Route::get('/metode-tindakan-list', "ActionMethod@list")->name('actionMethodList');
    Route::delete('/metode-tindakan/{kode}', "ActionMethod@delete")->name('actionMethodDelete');
    Route::get('/metode-tindakan/modal/{kode}', "ActionMethod@edit_modal")->name('actionMethodEditModal');
    Route::post('/metode-tindakan/{kode}', "ActionMethod@update")->name('actionMethodUpdate');

});

Route::get('/clear-cache', function () {
    Artisan::call('cache:clear');
    //Artisan::call('route:cache');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    return "Cache is cleared";
});

