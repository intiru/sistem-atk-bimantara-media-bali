<?php

namespace app\Rules;

use app\Helpers\Main;
use app\Models\mBarang;
use app\Models\mStokBarang;
use Illuminate\Contracts\Validation\Rule;
use app\Models\mUser;

/**
 * Digunakan untuk mengecheck apakah total stok barang tersedia sesuai dengan qty penjualan ?
 * dan jika pembelian barang lebih dari satu, dengan barang yang sama, proses dibawah ini akan memberikan proses validasinya
 *
 * Class rPenjualanQtyCheck
 * @package app\Rules
 */
class rPenjualanQtyCheck implements Rule
{
    private $value = '';
    private $request;
    private $brg_nama;
    private $brg_stok;
    private $pjl_qty_total;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $id_barang_arr = $this->request->id_barang;
        $pjd_qty_arr = $this->request->qty;
        $id_barang_unique = array_unique($id_barang_arr);
        $stok_barang_now_arr = [];
        $qty_jual_arr = [];
        $status_check = TRUE;



        foreach ($id_barang_unique as $id_barang) {
            $stok_barang_now_arr[$id_barang] = mStokBarang::where('id_barang', $id_barang)->sum('sbr_qty');
            $qty_jual_arr[$id_barang] = 0;
        }

        foreach ($pjd_qty_arr as $index => $qty) {
            $qty = Main::format_number_db($qty);
            $id_barang = $id_barang_arr[$index];
            $qty_jual_arr[$id_barang] = $qty_jual_arr[$id_barang] + $qty;
        }

        foreach ($stok_barang_now_arr as $id_barang => $stok_barang_now) {
            $qty_jual = $qty_jual_arr[$id_barang];
            if ($stok_barang_now < $qty_jual) {
                $this->brg_nama = mBarang::where('id_barang', $id_barang)->value('brg_nama');
                $this->brg_stok = $stok_barang_now;
                $this->pjl_qty_total = $qty_jual;
                $status_check =  FALSE;
                break;
            }
        }

        return $status_check;

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "Stok <strong>" . $this->brg_nama . "</strong> adalah " . Main::format_number($this->brg_stok) . "<br /><strong>Tidak Mencukupi</strong> Penjualan dengan total " . Main::format_number($this->pjl_qty_total) . ", harap check kembali <strong>Jumlah Stok</strong> yang tersedia";
    }
}
