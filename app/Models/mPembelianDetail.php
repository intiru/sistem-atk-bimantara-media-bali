<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mPembelianDetail extends Model
{
    use SoftDeletes;

    protected $table = 'pembelian_detail';
    protected $primaryKey = 'id_pembelian_detail';
    protected $fillable = [
        'id_pembelian',
        'id_barang',
        'id_satuan',
        'pbd_kode_batch',
        'pbd_harga_beli',
        'pbd_harga_jual',
        'pbd_expired',
        'pbd_ppn_persen',
        'pbd_ppn_nominal',
        'pbd_harga_net',
        'pbd_qty',
        'pbd_sub_total',
        'pbd_konsinyasi_status',
    ];

    public function pembelian() {
        return $this->belongsTo(mPembelian::class, 'id_pembelian');
    }

    public function barang() {
        return $this->belongsTo(mBarang::class, 'id_barang');
    }

    public function satuan() {
        return $this->belongsTo(mSatuan::class, 'id_satuan');
    }

    public function scopeWhereLike($query, $column, $value)
    {
        if ($value) {
            return $query->where($column, 'LIKE', '%' . $value . '%');
        }
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
        if ($value) {
            return $query->orWhere($column, 'LIKE', '%' . $value . '%');
        }
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
