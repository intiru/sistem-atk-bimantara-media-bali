<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mReturBarang extends Model
{
    use SoftDeletes;

    protected $table = 'retur_barang';
    protected $primaryKey = 'id_retur_barang';
    protected $fillable = [
        'id_penjualan',
        'id_pelanggan',
        'id_user',
        'rbr_urutan',
        'rbr_no_faktur',
        'rbr_tanggal_retur',
        'rbr_jenis_pembayaran',
        'rbr_total',
        'rbr_biaya_tambahan',
        'rbr_potongan',
        'rbr_grand_total',
        'rbr_jumlah_bayar',
        'rbr_sisa_pembayaran',
        'rbr_jumlah_pengembalian_uang',
        'rbr_jumlah_pengembalian_uang_penjelasan',
        'rbr_keterangan',
        'rbr_tanggal_jatuh_tempo',
    ];

    public function penjualan()
    {
        return $this->belongsTo(mPenjualan::class, 'id_penjualan');
    }

    public function pelanggan()
    {
        return $this->belongsTo(mPelanggan::class, 'id_pelanggan');
    }

    public function user()
    {
        return $this->belongsTo(mUser::class, 'id_user');
    }

    public function retur_barang_detail()
    {
        return $this->hasMany(mReturBarangDetail::class, 'id_retur_barang');
    }

    public function scopeWhereLike($query, $column, $value)
    {
        if ($value) {
            return $query->where($column, 'LIKE', '%' . $value . '%');
        }
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
        if ($value) {
            return $query->orWhere($column, 'LIKE', '%' . $value . '%');
        }
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
