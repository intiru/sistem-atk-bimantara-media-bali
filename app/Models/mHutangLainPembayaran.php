<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mHutangLainPembayaran extends Model
{
    use SoftDeletes;

    protected $table = 'hutang_lain_pembayaran';
    protected $primaryKey = 'id_hutang_lain_pembayaran';
    protected $fillable = [
        'id_hutang_lain',
        'id_user',
        'hlp_total_hutang',
        'hlp_jumlah_bayar',
        'hlp_sisa_bayar',
        'hlp_tanggal_bayar',
        'hlp_keterangan',
    ];

    public function hutang_lain()
    {
        return $this->belongsTo(mHutangLain::class, 'id_hutang_lain');
    }

    public function user()
    {
        return $this->belongsTo(mUser::class, 'id_user');
    }

    public function scopeWhereLike($query, $column, $value)
    {
        if ($value) {
            return $query->where($column, 'LIKE', '%' . $value . '%');
        }
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
        if ($value) {
            return $query->orWhere($column, 'LIKE', '%' . $value . '%');
        }
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
