<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mPenjualanDetail extends Model
{
    use SoftDeletes;

    protected $table = 'penjualan_detail';
    protected $primaryKey = 'id_penjualan_detail';
    protected $fillable = [
        'id_penjualan',
        'id_barang',
        'id_stok_barang',
        'id_stok_barang_json',
        'pjd_qty',
        'pjd_qty_sisa',
        'pjd_harga_jual',
        'pjd_hpp',
        'pjd_potongan',
        'pjd_ppn_persen',
        'pjd_ppn_nominal',
        'pjd_harga_net',
        'pjd_potongan_harga_barang',
        'pjd_sub_total',
    ];

    public function penjualan() {
        return $this->belongsTo(mPenjualan::class, 'id_penjualan');
    }

    public function barang() {
        return $this->belongsTo(mBarang::class, 'id_barang');
    }

    public function stok_barang() {
        return $this->belongsTo(mStokBarang::class, 'id_stok_barang');
    }

    public function scopeWhereLike($query, $column, $value)
    {
        if ($value) {
            return $query->where($column, 'LIKE', '%' . $value . '%');
        }
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
        if ($value) {
            return $query->orWhere($column, 'LIKE', '%' . $value . '%');
        }
    }

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }
    
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
