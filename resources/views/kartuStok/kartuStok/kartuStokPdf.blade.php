<head>
    <title>KARTU STOK {{ $date_from.' sampai '.$date_to }}</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nota.css') }}">
    <script type="text/javascript" src="{{ asset('js/print_page.js') }}"></script>
</head>
<body>
@include('component.kopSurat')
<h2 align="center">KARTU STOK</h2>
<center>
    Tanggal : {{ $date_from.' sampai '.$date_to }}
</center>
<br/>
<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th width="20" rowspan="2" class="text-center">No</th>
        <th rowspan="2" class="text-center">Tanggal</th>
        <th rowspan="2" class="text-center">Kode Barang</th>
        <th rowspan="2" class="text-center">Nama Barang</th>
        <th colspan="3" class="text-center">Stok</th>
{{--        <th rowspan="2" class="text-center">Sisa Barang</th>--}}
        <th rowspan="2" class="text-center">Sisa Total Barang</th>
        <th rowspan="2" class="text-center">Keterangan</th>
        <th rowspan="2" class="text-center">By</th>
    </tr>
    <tr>
        <th class="text-center">Awal</th>
        <th class="text-center">Masuk</th>
        <th class="text-center">Keluar</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data_list as $key => $row)
        <tr>
            <td align="center" class="string">{{ ++$key }}</td>
            <td class="string">{{ $row->arus_stok_date }}</td>
            <td class="string">{{ $row->brg_kode }}</td>
            <td class="string">{{ $row->brg_nama }}</td>
            <td class="string" align="right">{{ intval($row->asb_stok_awal) }}</td>
            <td class="string" align="right">{{ intval($row->asb_stok_masuk) }}</td>
            <td class="string" align="right">{{ intval($row->asb_stok_keluar) }}</td>
{{--            <td class="string" align="right">{{ intval($row->asb_stok_terakhir) }}</td>--}}
            <td class="string" align="right">{{ intval($row->asb_stok_total_terakhir) }}</td>
            <td class="string">{{ $row->asb_keterangan }}</td>
            <td class="string">{{ $row->nama_karyawan }}</td>
        </tr>
    @endforeach
    </tbody>
</table>



