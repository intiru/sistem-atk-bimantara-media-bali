<head>
    <title>PENYESUAIAN STOK {{ $date_from.' - '.$date_to }}</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nota.css') }}">
    <script type="text/javascript" src="{{ asset('js/print_page.js') }}"></script>
</head>
<body>
@include('component.kopSurat')
<h2 align="center">PENYESUAIAN STOK</h2>
<center>
    Tanggal : {{ $date_from.' sampai '.$date_to }}
</center>
<br/>
<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th width="20">No</th>
        <th>Tanggal</th>
        <th>Kode Barang</th>
        <th>Nama Barang</th>
        <th>Satuan</th>
        <th>Qty Awal</th>
        <th>Qty Akhir</th>
        <th>Keterangan</th>
        <th>Oleh</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data_list as $row)
        <tr>
            <td>{{ $no++ }}</td>
            <td>{{ Main::format_datetime($row->history_penyesuaian_stok_created_at) }}</td>
            <td>{{ $row->brg_kode }}</td>
            <td>{{ $row->brg_nama }}</td>
            <td>{{ $row->stn_nama }}</td>
            <td align="right">{{ Main::format_number($row->hps_qty_awal) }}</td>
            <td align="right">{{ Main::format_number($row->hps_qty_akhir) }}</td>
            <td>{{ $row->hps_keterangan }}</td>
            <td>{{ $row->nama_karyawan }}</td>
        </tr>
    @endforeach
    </tbody>
</table>



