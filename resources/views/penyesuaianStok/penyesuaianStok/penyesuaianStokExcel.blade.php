@php
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=Penyesuaian Stok ".$date_from." sampai ".$date_to.".xls");
@endphp

<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nota.css') }}">
</head>
<body>
@include('component.kopSuratExcel')
<h2 align="center">PENYESUAIAN STOK</h2>
<center>
    Tanggal : {{ $date_from.' sampai '.$date_to }}
</center>
<br/>
<table border="1" width="100%">
    <thead>
    <tr>
        <th width="20">No</th>
        <th>Tanggal</th>
        <th>Kode Barang</th>
        <th>Nama Barang</th>
        <th>Satuan</th>
        <th>Qty Awal</th>
        <th>Qty Akhir</th>
        <th>Keterangan</th>
        <th>Oleh</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data_list as $row)
        <tr>
            <td class="string">{{ $no++ }}</td>
            <td class="string">{{ Main::format_datetime($row->history_penyesuaian_stok_created_at) }}</td>
            <td class="string">{{ $row->brg_kode }}</td>
            <td class="string">{{ $row->brg_nama }}</td>
            <td class="string">{{ $row->stn_nama }}</td>
            <td class="string" align="right">{{ $row->hps_qty_awal }}</td>
            <td class="string" align="right">{{ $row->hps_qty_akhir }}</td>
            <td class="string">{{ $row->hps_keterangan }}</td>
            <td class="string">{{ $row->nama_karyawan }}</td>
        </tr>
    @endforeach
    </tbody>
</table>



