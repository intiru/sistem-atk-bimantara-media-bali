<div class="modal" id="modal-general" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-xxlg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail Retur Barang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="m-portlet__body detail-info">
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Nama Pelanggan</label>
                        <div class="col-9 col-form-label">
                            {{ $retur_barang->pelanggan->plg_nama }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Alamat Pelanggan</label>
                        <div class="col-9 col-form-label">
                            {{ $retur_barang->pelanggan->plg_alamat }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Admin yang Melakukan Retur Barang</label>
                        <div class="col-9 col-form-label">
                            {{ $retur_barang->user->karyawan->nama_karyawan }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Keterangan Retur Barang</label>
                        <div class="col-9 col-form-label">
                            {{ $retur_barang->rbr_keterangan }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">No Faktur Retur</label>
                        <div class="col-9 col-form-label">
                            {{ $retur_barang->rbr_no_faktur }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Tanggal Retur</label>
                        <div class="col-9 col-form-label">
                            {{ Main::format_date($retur_barang->rbr_tanggal_retur) }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Jenis Pembayaran</label>
                        <div class="col-9 col-form-label">
                            {{ $retur_barang->rbr_jenis_pembayaran }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Grand Total</label>
                        <div class="col-9 col-form-label">
                            {{ Main::format_number($retur_barang->rbr_grand_total) }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Jumlah Pengembalian Uang Retur Barang</label>
                        <div class="col-9 col-form-label">
                            {{ Main::format_number($retur_barang->rbr_jumlah_pengembalian_uang) }}
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="example-text-input" class="col-3 col-form-label">Alasan Pengembalian Uang Retur Barang</label>
                        <div class="col-9 col-form-label">
                            {{ $retur_barang->rbr_jumlah_pengembalian_uang_penjelasan }}
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="datatable table table-striped table-bordered table-hover table-checkable datatable-general">
                            <thead>
                            <tr>
                                <th width="20">No</th>
                                <th>Kode Barang</th>
                                <th>Nama Barang</th>
                                <th>Qty Retur</th>
                                <th>Harga</th>
                                <th>Potongan Harga</th>
                                <th>Sub Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($retur_barang->retur_barang_detail as $key => $row)
                                <tr>
                                    <td>{{ ++$key }}.</td>
                                    <td>{{ $row->barang->brg_kode }}</td>
                                    <td>{{ $row->barang->brg_nama }}</td>
                                    <td>{{ Main::format_number($row->rbd_qty) }}</td>
                                    <td>{{ Main::format_money($row->rbd_harga_jual) }}</td>
                                    <td>{{ Main::format_money($row->rbd_potongan_harga_barang) }}</td>
                                    <td>{{ Main::format_money($row->rbd_sub_total) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>