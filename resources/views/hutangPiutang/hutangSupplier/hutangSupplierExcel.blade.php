@php
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=Hutang Supplier Belum Lunas ".$date_from." sampai ".$date_to.".xls");
@endphp

<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nota.css') }}">
</head>
<body>
@include('component.kopSuratExcel')
<h2 align="center">HUTANG SUPPLIER - BELUM LUNAS</h2>
<center>
    Tanggal : {{ $date_from.' sampai '.$date_to }}
</center>
<br/>
<table border="1" width="100%">
    <thead>
    <tr>
        <th width="20">No</th>
        <th>Faktur Hutang</th>
        <th>Faktur Pembelian</th>
        <th>Nama Supplier</th>
        <th>Tanggal Hutang</th>
        <th>Jatuh Tempo</th>
        <th>Total Hutang</th>
        <th>Sisa</th>
        <th>Keterangan</th>
    </tr>
    </thead>
    <tbody>
    @php
        $total_hutang_supplier = 0;
        $total_hutang_supplier_sisa = 0;
    @endphp
    @foreach($data_list as $row)
        @php
            $total_hutang_supplier += $row->hsp_total;
            $total_hutang_supplier_sisa += $row->hsp_sisa;
        @endphp
        <tr>
            <td class="string">{{ $no++ }}</td>
            <td class="string">{{ $row->hsp_no_faktur }}</td>
            <td class="string">{{ $row->pbl_no_faktur }}</td>
            <td class="string">{{ $row->spl_nama }}</td>
            <td class="string">{{ Main::format_date($row->hsp_tanggal) }}</td>
            <td class="string">{{ Main::format_date($row->hsp_tanggal_jatuh_tempo) }}</td>
            <td class="string" align="right">{{ intval($row->hsp_total) }}</td>
            <td class="string" align="right">{{ intval($row->hsp_sisa) }}</td>
            <td class="string">{{ $row->hsp_keterangan }}</td>
        </tr>
    @endforeach
        <tr>
            <td colspan="6" align="center"><strong>TOTAL</strong></td>
            <td align="right"><strong>{{ intval($total_hutang_supplier) }}</strong></td>
            <td align="right"><strong>{{ intval($total_hutang_supplier_sisa) }}</strong></td>
            <td></td>
        </tr>
    </tbody>
</table>



