@php
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=Hutang Lain Belum Lunas ".$date_from." sampai ".$date_to.".xls");
@endphp

<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nota.css') }}">
</head>
<body>
@include('component.kopSuratExcel')
<h2 align="center">HUTANG LAIN - BELUM LUNAS</h2>
<center>
    Tanggal : {{ $date_from.' sampai '.$date_to }}
</center>
<br/>
<table border="1" width="100%">
    <thead>
    <tr>
        <th width="20">No</th>
        <th>Faktur Hutang</th>
        <th>Tanggal Hutang</th>
        <th>Jatuh Tempo</th>
        <th>Total Hutang</th>
        <th>Sisa</th>
        <th>Keterangan</th>
    </tr>
    </thead>
    <tbody>
    @php
        $total_hutang_lain = 0;
        $total_hutang_lain_sisa = 0;
    @endphp
    @foreach($data_list as $row)
        @php
            $total_hutang_lain += $row->hul_total;
            $total_hutang_lain_sisa += $row->hul_sisa;
        @endphp
        <tr>
            <td class="string">{{ $no++ }}</td>
            <td class="string">{{ $row->hul_no_faktur }}</td>
            <td class="string">{{ Main::format_date($row->hul_tanggal) }}</td>
            <td class="string">{{ Main::format_date($row->hul_tanggal_jatuh_tempo) }}</td>
            <td class="string" align="right">{{ $row->hul_total }}</td>
            <td class="string" align="right">{{ $row->hul_sisa }}</td>
            <td class="string">{{ $row->hul_keterangan }}</td>
        </tr>
    @endforeach
        <tr>
            <td colspan="4" align="center"><strong>TOTAL</strong></td>
            <td align="right"><strong>{{ intval($total_hutang_lain) }}</strong></td>
            <td align="right"><strong>{{ intval($total_hutang_lain_sisa) }}</strong></td>
            <td></td>
        </tr>
    </tbody>
</table>



