@php
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=Dashboard Sistem ".$date_from." sampai ".$date_to.".xls");
@endphp

<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nota.css') }}">
</head>
<body>
@include('component.kopSuratExcel')
<h2 align="center">DASHBOARD SISTEM</h2>
<center>
    Tanggal : {{ $date_from.' sampai '.$date_to }}
</center>
<br/>
<table border="1" width="100%">
    <tbody>
    <tr>
        <td>Profit</td>
        <td class="string">{{ $total_profit }}</td>
    </tr>
    <tr>
        <td>Total Pembelian</td>
        <td class="string">{{ $total_pembelian }}</td>
    </tr>
    <tr>
        <td>Total Penjualan</td>
        <td class="string">{{ $total_penjualan }}</td>
    </tr>
    <tr>
        <td>Total Stok Alert</td>
        <td class="string">{{ $total_stok_alert }}</td>
    </tr>
    <tr>
        <td>Total Hutang Supplier</td>
        <td class="string">{{ $total_hutang_supplier }}</td>
    </tr>
    <tr>
        <td>Total Piutang Pelanggan</td>
        <td class="string">{{ $total_piutang_pelanggan }}</td>
    </tr>
    <tr>
        <td>Total Barang</td>
        <td class="string">{{ $total_barang }}</td>
    </tr>
    <tr>
        <td>Total Pelanggan</td>
        <td class="string">{{ $total_pelanggan }}</td>
    </tr>
    <tr>
        <td>Total Supplier</td>
        <td class="string">{{ $total_supplier }}</td>
    </tr>
    </tbody>
</table>



