<head>
    <title>DASHBOARD SISTEM {{ $date_from.' - '.$date_to }}</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nota.css') }}">
    <script type="text/javascript" src="{{ asset('js/print_page.js') }}"></script>
</head>
<body>
@include('component.kopSurat')
<h2 align="center">DASHBOARD SISTEM</h2>
<center>
    Tanggal : {{ $date_from.' sampai '.$date_to }}
</center>
<br/>
<table class="table table-bordered table-striped">
    <tbody>
    <tr>
        <td>Profit</td>
        <td class="string" align="right">{{ Main::format_number($total_profit) }}</td>
    </tr>
    <tr>
        <td>Total Pembelian</td>
        <td class="string" align="right">{{ Main::format_number($total_pembelian) }}</td>
    </tr>
    <tr>
        <td>Total Penjualan</td>
        <td class="string" align="right">{{ Main::format_number($total_penjualan) }}</td>
    </tr>
    <tr>
        <td>Total Stok Alert</td>
        <td class="string" align="right">{{ Main::format_number($total_stok_alert) }}</td>
    </tr>
    <tr>
        <td>Total Hutang Supplier</td>
        <td class="string" align="right">{{ Main::format_number($total_hutang_supplier) }}</td>
    </tr>
    <tr>
        <td>Total Piutang Pelanggan</td>
        <td class="string" align="right">{{ Main::format_number($total_piutang_pelanggan) }}</td>
    </tr>
    <tr>
        <td>Total Barang</td>
        <td class="string" align="right">{{ Main::format_number($total_barang) }}</td>
    </tr>
    <tr>
        <td>Total Pelanggan</td>
        <td class="string" align="right">{{ Main::format_number($total_pelanggan) }}</td>
    </tr>
    <tr>
        <td>Total Supplier</td>
        <td class="string" align="right">{{ Main::format_number($total_supplier) }}</td>
    </tr>
    </tbody>
</table>



