@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">

            {!! $date_filter !!}

            <div class="m-portlet m-portlet--bordered-semi m-portlet--space m-portlet--full-height ">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                PROFIT :
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">

                    <div class="m-widget25">
                        @if($profit < 0)
                            <span class="m-widget25__price m--font-danger">
                                {{ \app\Helpers\Main::format_money($profit) }}
                            </span>
                        @else
                            <span class="m-widget25__price m--font-accent">
                                {{ \app\Helpers\Main::format_money($profit) }}
                            </span>
                        @endif
                        <span class="m-widget25__desc">
                            Total Profit sesuai dengan filter tanggal |
                            <small><i>formula : (penjualan - pembelian)</i></small>
                        </span>
                        <div class="m-widget25--progress">
                            <div class="m-widget25__progress">
                                <span class="m-widget25__progress-number">
                                    {{ \app\Helpers\Main::format_number($pembelian_total) }}
                                </span>
                                <div class="m--space-10"></div>
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-success" role="progressbar" style="width: 100%;"
                                         aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget25__progress-sub">
                                    PEMBELIAN
                                </span>
                            </div>
                            <div class="m-widget25__progress">
                                <span class="m-widget25__progress-number">
                                    {{ \app\Helpers\Main::format_number($penjualan_total) }}
                                </span>
                                <div class="m--space-10"></div>
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-warning" role="progressbar" style="width: 100%;"
                                         aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget25__progress-sub">
                                    PENJUALAN (OMSET)
                                </span>
                            </div>
                            <div class="m-widget25__progress">
                                <span class="m-widget25__progress-number">
                                    {{ \app\Helpers\Main::format_number($hutang_total) }}
                                </span>
                                <div class="m--space-10"></div>
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-accent" role="progressbar" style="width: 100%;"
                                         aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget25__progress-sub">
                                    HUTANG
                                </span>
                            </div>
                            <div class="m-widget25__progress">
                                <span class="m-widget25__progress-number">
                                    {{ \app\Helpers\Main::format_number($piutang_total) }}
                                </span>
                                <div class="m--space-10"></div>
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-info" role="progressbar" style="width: 100%;"
                                         aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget25__progress-sub">
                                    PIUTANG
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 0">

                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#tab-pembelian" aria-expanded="true">
                        <h4><i class="flaticon-shopping-basket"></i> Pembelian</h4>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#tab-penjualan" aria-expanded="true">
                        <h4><i class="flaticon-bell"></i>Penjualan</h4>
                    </a>
                </li>
            </ul>
            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-pembelian" role="tabpanel" aria-expanded="true">
                        <div class="m-portlet__body">
                            <table class="table table-bordered datatable-new"
                                   data-url="{{ route('laporanPembelianDataTable') }}"
                                   data-column="{{ json_encode($datatable_column_pembelian) }}"
                                   data-data="{{ json_encode($table_data_post) }}">
                                <thead>
                                <tr>
                                    <th width="20">No</th>
                                    <th>Tanggal</th>
                                    <th>Nama Barang</th>
                                    <th>Harga Beli</th>
                                    <th>Qty</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-penjualan" role="tabpanel" aria-expanded="true">
                        <div class="m-portlet__body">
                            <table class="table table-bordered datatable-new"
                                   data-url="{{ route('laporanPenjualanDataTable') }}"
                                   data-column="{{ json_encode($datatable_column_penjualan) }}"
                                   data-data="{{ json_encode($table_data_post) }}">
                                <thead>
                                <tr>
                                    <th width="20">No</th>
                                    <th>Tanggal</th>
                                    <th>Nama Barang</th>
                                    <th>Qty</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </div>
@endsection
