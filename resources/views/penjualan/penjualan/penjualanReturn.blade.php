@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
    <style type="text/css">
        .input-group.bootstrap-touchspin > .input-group-btn {
            display: none !important;
        }
    </style>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/datatables/basic/paginations.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datetimepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('js/penjualan.js') }}" type="text/javascript"></script>
@endsection

@section('body')

    <form action="{{ route('penjualanReturnProcess', ['id_penjualan' => \app\Helpers\Main::encrypt($penjualan->id_penjualan)]) }}"
          method="post"
          style="width: 100%"
          data-redirect="{{ route('penjualanList') }}"
          data-alert-show="true"
          data-alert-field-message="true"
          data-confirm-status="true"
          data-confirm-message="Jika Return Barang Penjualan sudah selesai di proses, maka edit data return tidak bisa
          dilakukan secara langsung, namun melalui penyesuaian stok, piutang pelanggan dan menu lainnya yang berkaitan"
          class="form-send">

        {{ csrf_field() }}

        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title m-subheader__title--separator">
                            {{ $pageTitle }}
                        </h3>
                        {!! $breadcrumb !!}
                    </div>
                </div>
            </div>
            <div class="m-content">

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon">
                                <i class="flaticon-information"></i>
                            </span>
                                <h4 class="m-portlet__head-text">
                                    Informasi Penjualan
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body row">
                        <div class="col-lg-6">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Tanggal Penjualan</label>
                                <div class="col-6 col-form-label">
                                    {{ Main::format_datetime($penjualan->pjl_tanggal_penjualan) }}
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Pelanggan</label>
                                <div class="col-8 col-form-label">
                                    {{ '( '.$penjualan->pelanggan->plg_kode.') '.$penjualan->pelanggan->plg_nama }}
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Nomer Telepon</label>
                                <div class="col-2">
                                    <div class="input-group col-form-label telp_supplier">{{ $penjualan->pelanggan->plg_telepon }}</div>
                                </div>
                                <label for="example-text-input" class="col-2 col-form-label">Alamat</label>
                                <div class="col-4">
                                    <div class="input-group col-form-label alamat_supplier">{{ $penjualan->pelanggan->plg_alamat }}</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">No Faktur Penjualan</label>
                                <div class="col-9 col-form-label no_faktur">{{ $penjualan->pjl_no_faktur }}</div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Jenis Pembayaran</label>
                                <div class="col-6 col-form-label">
                                    {{ $penjualan->pjl_jenis_pembayaran }}
                                </div>
                            </div>

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-3 col-form-label">Keterangan/Catatan</label>
                                <div class="col-6 col-form-label">
                                    {{ $penjualan->pjl_keterangan }}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="flaticon-cart"></i>
                                </span>
                                <h4 class="m-portlet__head-text">
                                    Data Barang Return
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <table class="table-bahan table table-striped table-bordered table-hover table-checkable">
                            <thead>
                            <tr>
                                <th align="center">Nama Barang</th>
                                {{--                                <th>Stok Barang</th>--}}
                                <th>Harga Jual</th>
                                <th class="m--hide">PPN({{ $ppnPersen }}%)</th>
                                <th class="m--hide">Harga Net</th>
                                <th>Stok</th>
                                <th>Qty</th>
                                <th>Potongan Harga</th>
                                <th>Subtotal</th>
                                <th width="100">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-lg-5 offset-lg-7">
                        <div class="m-portlet m-portlet--mobile">
                            <div class="m-portlet__body row">
                                <div class="col-lg-12">
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Total</label>
                                        <div class="col-8 col-form-label total">0</div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Biaya
                                            Tambahan</label>
                                        <div class="col-8">
                                            <input class="form-control m-input input-numeral" type="text"
                                                   name="biaya_tambahan" value="0">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Potongan</label>
                                        <div class="col-8">
                                            <input class="form-control m-input input-numeral" type="text"
                                                   name="potongan" value="0">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Grand Total</label>
                                        <div class="col-8 col-form-label grand-total"
                                             style="font-size: 20px; font-weight: bold">0
                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Jumlah
                                            Bayar</label>
                                        <div class="col-8">
                                            <input class="form-control m-input input-numeral" type="text"
                                                   name="jumlah_bayar" value="0">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-4 col-form-label">Sisa
                                            (Piutang)</label>
                                        <div class="col-8 col-form-label sisa-pembayaran"
                                             style="font-size: 20px; font-weight: bold">0
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row div-jatuh-tempo m--hide">
                                        <label for="example-text-input" class="col-4 col-form-label">Tanggal Jatuh
                                            Tempo</label>
                                        <div class="col-8">
                                            <input type="text"
                                                   name="tanggal_jatuh_tempo"
                                                   class="form-control m-input m_datepicker_top"
                                                   readonly=""
                                                   value="{{ date('d-m-Y') }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


        <div class="produksi-buttons">
            <button type="submit"
                    class="btn btn-primary btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-check"></i>
                            <span>Simpan Pembelian</span>
                        </span>
            </button>

            <a href="{{ route('penjualanList') }}"
               class="btn-produk-add btn btn-warning btn-lg m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-angle-double-left"></i>
                            <span>Kembali</span>
                        </span>
            </a>
        </div>

    </form>
@endsection