<div class="kop-surat">
    <div class="logo">
        <img src="{{ asset('images/logo-sistem-atk.jpg') }}" width="114">
    </div>
    <div class="judul">
        <h1>CV. BIMANTARA MEDIA BALI</h1>
        <h4>DISTRIBUTOR BUKU PELAJARAN, BAHAN & ALAT LABORATORIUM</h4>
        <h4>ALAT TULIS KANTOR, ALAT PERAGA PENDIDIKAN, PERALATAN & PERLENGKAPAN IT</h4>
        <h4>KANTOR DENPASAR: JL. GUNUNG SLAMET NO. 11 TEGAL HARUM, DENPASAR</h4>
        <h4>NO. HP : 0813-5859-4788</h4>
    </div>
    <div class="clearfix"></div>
</div>
<hr/>
<br/>