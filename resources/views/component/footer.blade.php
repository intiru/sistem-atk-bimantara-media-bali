<footer class="m-grid__item	m-footer ">
    <div class="m-container m-container--fluid m-container--full-height m-page__container">
        <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
            <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
                <span class="m-footer__copyright">
                    2019 - {{ date('Y') }} &copy; Development by <a href="https://mahendrawardana.com" target="_blank">Mahendra Wardana</a> | No. WhatsApp / Phone : <a href="https://wa.me/6281237376068?text=Hai%2C%0D%0ATerima+Kasih+sudah+berkunjung+%0D%0AMohon+isi+form+dibawah.%0D%0ANama+%3A+%0D%0ANama+Kota+%3A+%0D%0APertanyaan+%3A+Tentang+Produk+Program+ATK" target="_blank">0812-3737-6068</a>
                </span>
            </div>
        </div>
    </div>
</footer>