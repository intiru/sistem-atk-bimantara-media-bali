@php
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=Rekap Pembelian ".$date_from." sampai ".$date_to.".xls");
@endphp

<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nota.css') }}">
</head>
<body>
@include('component.kopSuratExcel')
<h2 align="center">REKAP PEMBELIAN</h2>
<center>
    Tanggal : {{ $date_from.' sampai '.$date_to }}
</center>
<br/>
<table border="1" width="100%">
    <thead>
    <tr>
        <th width="40">No</th>
        <th>No Faktur Pembelian</th>
        <th>No Faktur Supplier</th>
        <th>Nama Supplier</th>
        <th>Tanggal Beli</th>
        <th>Jenis Pembayaran</th>
        <th>Qty</th>
        <th>Grand Total</th>
    </tr>
    </thead>
    <tbody>
    @php
        $total_qty = 0;
        $total_grand_total = 0;
    @endphp
    @foreach($data_pembelian as $key => $row)
        @php
            $total_qty += $row->total_qty;
            $total_grand_total += $row->pbl_grand_total;
        @endphp
        <tr>
            <td align="center" class="string">{{ ++$key }}</td>
            <td class="string">{{ $row->pbl_no_faktur }}</td>
            <td class="string">{{ $row->pbl_no_faktur_supplier }}</td>
            <td class="string">{{ $row->spl_nama }}</td>
            <td class="string">{{ $row->pbl_tanggal_order }}</td>
            <td class="string" align="center">{{ $row->pbl_jenis_pembayaran }}</td>
            <td class="string" align="right">{{ intval($row->total_qty) }}</td>
            <td class="string" align="right">{{ intval($row->pbl_grand_total) }}</td>
        </tr>
    @endforeach
        <tr>
            <td colspan="6" align="center"><strong>TOTAL</strong></td>
            <td align="right"><strong>{{ intval($total_qty) }}</strong></td>
            <td align="right"><strong>{{ intval($total_grand_total) }}</strong></td>
        </tr>
    </tbody>
</table>



