<head>
    <title>REKAP STOK ALERT BARANG</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/nota.css') }}">
    <script type="text/javascript" src="{{ asset('js/print_page.js') }}"></script>
</head>
<body>
@include('component.kopSurat')
<h2 align="center">STOK ALERT BARANG</h2>
<br/>
<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th width="20">No</th>
        <th>Kode Barang</th>
        <th>Nama Barang</th>
        <th>Jenis</th>
        <th>Golongan</th>
        <th>Minimal Stok</th>
        <th>Total Stok</th>
    </tr>
    </thead>
    <tbody>
    @php
        $total_minimal_stok = 0;
        $total_stok_all = 0;
    @endphp
    @foreach($data_list as $key => $row)
        @php
            $total_stok = \app\Models\mStokBarang::where('id_barang', $row->id_barang)->sum('sbr_qty');
        @endphp
        @if($row->brg_minimal_stok >= $total_stok)
            @php
                $total_minimal_stok += $row->brg_minimal_stok;
                $total_stok_all += $total_stok;
            @endphp
            <tr>
                <td align="center" class="string">{{ $no++ }}</td>
                <td class="string">{{ $row->brg_kode }}</td>
                <td class="string">{{ $row->brg_nama }}</td>
                <td class="string">{{ $row->jbr_nama }}</td>
                <td class="string">{{ $row->brg_golongan }}</td>
                <td class="string" align="right">{{ Main::format_number($row->brg_minimal_stok) }}</td>
                <td class="string" align="right">{{ Main::format_number($total_stok) }}</td>
            </tr>
        @endif
    @endforeach
    <tr>
        <td align="center" colspan="5"><strong>TOTAL</strong></td>
        <td align="right"><strong>{{ Main::format_number($total_minimal_stok) }}</strong></td>
        <td align="right"><strong>{{ Main::format_number($total_stok_all) }}</strong></td>
    </tr>
    </tbody>
</table>



